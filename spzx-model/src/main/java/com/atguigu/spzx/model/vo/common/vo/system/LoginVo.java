package com.atguigu.spzx.model.vo.common.vo.system;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginVo {

      /*
      * token
      * */
    private String token;

    /*
    * 刷新令牌
    * */
    private String refreshToken;


}
