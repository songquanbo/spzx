package com.atguigu.spzx.manager.service.impl;

import com.atguigu.spzx.exception.BaseException;
import com.atguigu.spzx.manager.properties.UploadProperties;
import com.atguigu.util.MinioUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadService implements com.atguigu.spzx.manager.service.IUploadService {
    @Autowired
    private UploadProperties uploadProperties;

    /**
     * 文件上传
     * @param file
     * @return
     */
    public String fileUpload(MultipartFile file) {
        String path="";
        try {
            path = MinioUploadUtil.upload(file, uploadProperties.getEndpointUrl(),
                    uploadProperties.getAssceeKey(), uploadProperties.getSecretKey(), uploadProperties.getBucketName());
        } catch (Exception e) {
            throw new BaseException("上传失败");
        }
        return path;
    }
}
