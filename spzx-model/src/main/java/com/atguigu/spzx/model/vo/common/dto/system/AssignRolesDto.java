package com.atguigu.spzx.model.vo.common.dto.system;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class AssignRolesDto {
    @NotNull
    private Long userId;
    @NotEmpty
    private List<Long> roleIds;
}
