package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.system.SysOperLog;

public interface SysLogMapper {


   void saveOperationLog(SysOperLog sysOperLog) ;

}
