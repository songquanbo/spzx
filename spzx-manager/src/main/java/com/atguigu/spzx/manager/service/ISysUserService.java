package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.dto.system.LoginDto;
import com.atguigu.spzx.model.vo.common.dto.system.SysUserDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysRole;
import com.atguigu.spzx.model.vo.common.entity.system.SysUser;
import com.atguigu.spzx.model.vo.common.vo.system.CaptchaVo;
import com.atguigu.spzx.model.vo.common.vo.system.LoginVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 宋哥
 * @since 2023-12-25
 */
public interface ISysUserService extends IService<SysUser> {

    LoginVo login(LoginDto loginDto);


    CaptchaVo generateValidateCode();

    void logout(String token);

    Page<SysUser> selectUserByPage(Integer pageNum, Integer pageSize, SysUserDto sysUserDto);

    void saveSysUser(SysUser sysUser);

    void updateSysUser(SysUser sysUser);

    void deleteById(Long id);

    Map<String,Object> showRolesOfUser(Long userId);
}
