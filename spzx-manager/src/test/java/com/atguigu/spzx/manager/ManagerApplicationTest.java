package com.atguigu.spzx.manager;


import org.junit.jupiter.api.Test;

public class ManagerApplicationTest {

    @Test
    public void md5Test(){
//        TreeMap<Integer,Integer> map = new TreeMap<>();
//        List<Integer> list = map.entrySet()
//                .stream()
//                .sorted(Comparator.comparingInt(Map.Entry::getValue))
//                .map(Map.Entry::getKey).toList();
//        System.out.println(list);
//        testCompletePack();
//        Solution solution = new Solution();
//        solution.maxProfit(2,new int[]{2,4,1});
        new Solution1().findLengthOfLCIS(new int[]{1,3,5,4,7});
    }
    class Solution {
        public int maxProfit(int k, int[] prices) {
            int len=prices.length;
            int[][] dp =new int[len+1][2*k+1];
            //初始化
            for(int i=1;i<=2*k;i+=2){
                dp[1][i]=-prices[0];
            }
            for(int i=2;i<=len;i++){
                for(int j=1;j<=2*k;j++){
                    if((j&1)!=0) dp[i][k]=Math.max(dp[i-1][k],dp[i-1][k-1]-prices[i-1]);//k是奇数，表示持有股票
                    else dp[i][k] = Math.max(dp[i-1][k],dp[i-1][k-1]+prices[i-1]);
                }
            }
            return dp[len][2*k];
        }
    }
    private static void testCompletePack(){
        int[] weight = {1, 3, 4};
        int[] value = {15, 20, 30};
        int bagWeight = 4;
        int[] dp = new int[bagWeight + 1];
        for (int i = 0; i < weight.length; i++){ // 遍历物品
            for (int j = weight[i]; j <= bagWeight; j++){ // 遍历背包容量
                dp[j] = Math.max(dp[j], dp[j - weight[i]] + value[i]);
            }
        }
        for (int maxValue : dp){
            System.out.println(maxValue + "   ");
        }
    }

    //先遍历背包，再遍历物品
    private static void testCompletePackAnotherWay(){
        int[] weight = {1, 3, 4};
        int[] value = {15, 20, 30};
        int bagWeight = 4;
        int[] dp = new int[bagWeight + 1];
        for (int i = 1; i <= bagWeight; i++){ // 遍历背包容量
            for (int j = 0; j < weight.length; j++){ // 遍历物品
                if (i - weight[j] >= 0){
                    dp[i] = Math.max(dp[i], dp[i - weight[j]] + value[j]);
                }
            }
        }
        for (int maxValue : dp){
            System.out.println(maxValue + "   ");
        }
    }

}
class Solution1 {
    public  int findLengthOfLCIS(int[] nums) {
        int[] dp = new int[nums.length];
        for (int i = 0; i < dp.length; i++) {
            dp[i] = 1;
        }
        int res = 1;
        //可以注意到，這邊的 i 是從 0 開始，所以會出現和卡哥的C++ code有差異的地方，在一些地方會看到有 i + 1 的偏移。
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i + 1] > nums[i]) {
                dp[i + 1] = dp[i] + 1;
            }
            System.out.println(dp[i + 1]);
            res = res > dp[i + 1] ? res : dp[i + 1];
        }
        return res;
    }
}
