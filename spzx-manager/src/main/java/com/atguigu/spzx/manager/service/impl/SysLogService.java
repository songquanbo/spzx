package com.atguigu.spzx.manager.service.impl;

import com.atguigu.spzx.common.log.service.ISysLogService;
import com.atguigu.spzx.manager.mapper.SysLogMapper;
import com.atguigu.spzx.model.vo.common.entity.system.SysOperLog;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SysLogService implements ISysLogService {

    private final SysLogMapper sysLogMapper;
    /**
     * 添加操作日志
     * @param sysOperLog
     */
    public void saveOperationLog(SysOperLog sysOperLog) {
        sysLogMapper.saveOperationLog(sysOperLog);
    }
}
