package com.atguigu.spzx.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpzxGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpzxGatewayApplication.class,args);
    }
}
