package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.entity.product.ProductDetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品sku属性表 服务类
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
public interface IProductDetailsService extends IService<ProductDetails> {

}
