package com.atguigu.spzx.manager.service.impl;

import com.atguigu.spzx.manager.mapper.OrderStatisticsMapper;
import com.atguigu.spzx.manager.service.IOrderStatisticsService;
import com.atguigu.spzx.model.vo.common.entity.order.OrderStatistics;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单统计 服务实现类
 * </p>
 *
 * @author author
 * @since 2024-03-16
 */
@Service
@RequiredArgsConstructor
public class OrderStatisticsServiceImpl extends ServiceImpl<OrderStatisticsMapper, OrderStatistics> implements IOrderStatisticsService {

    private final OrderStatisticsMapper orderStatisticsMapper;
    /**
     * 插入订单统计数据
     * @param orderStatistics
     */
    public void addOrderStatistics(OrderStatistics orderStatistics) {
        orderStatisticsMapper.addOrderStatistics(orderStatistics);
    }
}
