package com.atguigu.spzx.common.log.annotation;

import com.atguigu.spzx.common.log.aspect.OperationLogAspect;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({OperationLogAspect.class})
public @interface EnableLogAspect {


}
