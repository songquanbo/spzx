package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.product.ProductUnit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品单位 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-03-14
 */
public interface ProductUnitMapper extends BaseMapper<ProductUnit> {

}
