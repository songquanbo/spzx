package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.product.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 商品分类 Mapper 接口
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-13
 */
public interface CategoryMapper extends BaseMapper<Category> {

    void saveBatch(List cachedDataList);
}
