package com.atguigu.spzx.manager.service.impl;

import com.atguigu.spzx.manager.mapper.SysRoleMenuMapper;
import com.atguigu.spzx.manager.service.ISysMenuService;
import com.atguigu.spzx.manager.service.ISysRoleMenuService;
import com.atguigu.spzx.model.vo.common.entity.system.SysMenu;
import com.atguigu.spzx.model.vo.common.entity.system.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色菜单 服务实现类
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-12
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

    /**
     * 回显当前角色id的菜单权限
     * @param userId
     * @return
     */
    @Autowired
    private ISysMenuService sysMenuService;

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;


    public Map<String, Object> findMenusWithRoleId(Long roleId) {
        //获取所有菜单
        List<SysMenu> allMenus = sysMenuService.findMenus();
        //获取当前用户id对应的菜单
       List<Long> menusId= sysRoleMenuMapper.findMenusIdByRoleId(roleId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("allMenus",allMenus);
        map.put("menusId",menusId);
        return map;
    }
}
