package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.dto.product.ProductSkuDto;
import com.atguigu.spzx.model.vo.common.entity.product.Product;
import com.atguigu.spzx.model.vo.common.vo.product.ProductVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 商品 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
public interface ProductMapper extends BaseMapper<Product> {

    List<ProductVo> page(Page<ProductVo> productPage, ProductSkuDto productSkuDto);

    void save(Product product);

    void updateByProductId(Product product);

    Product selectByProductId(Long id);
    @Update("update db_spzx.product set is_deleted=1 where id=#{id}")
    void delProduct(Long id);

}
