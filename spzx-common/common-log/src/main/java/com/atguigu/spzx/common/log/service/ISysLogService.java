package com.atguigu.spzx.common.log.service;

import com.atguigu.spzx.model.vo.common.entity.system.SysOperLog;

public interface ISysLogService {
    void saveOperationLog(SysOperLog sysOperLog);
}
