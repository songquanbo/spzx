package com.atguigu.spzx.manager.controller;


import com.atguigu.spzx.manager.service.IProductService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.dto.product.ProductDto;
import com.atguigu.spzx.model.vo.common.dto.product.ProductSkuDto;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import com.atguigu.spzx.model.vo.common.vo.product.ProductVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 商品 前端控制器
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {
    private final IProductService productService;

    /**
     * 分页条件查询
     * @param page
     * @param limit
     * @param productSkuDto
     * @return
     */
    @GetMapping("/list/{page}/{limit}")
    public Result<PageInfo<ProductVo>> list(@PathVariable Integer page, @PathVariable Integer limit, ProductSkuDto productSkuDto){
        return Result.success(productService.listPage(page,limit,productSkuDto));
    }

    /**
     * 保存商品数据
     * @param productDto
     * @return
     */
    @PostMapping("save")
    public Result addProduct(@RequestBody ProductDto productDto){
        productService.save(productDto);
        return Result.success(null,"添加成功");
    }
    //根据id查询商品数据
    @GetMapping("getByProductId/{id}")
    public Result<ProductDto> getByProductId(@PathVariable Long id){
        return Result.success(productService.getByProductId(id));
    }

    /**
     * 修改商品数据通过id
     * @param productDto
     * @return
     */
    @PutMapping("updateProductById")
    public Result updateProductById(@RequestBody ProductDto productDto){
        productService.updateProductById(productDto);
        return Result.success(null,"修改成功");
    }

    /**
     * 根据id删除商品信息
     * @param id
     * @return
     */
    @DeleteMapping("delProductById/{id}")
    public Result delProductById(@PathVariable Long id){
        productService.delProductById(id);
        return Result.success(null,"删除成功");
    }
    @GetMapping("/updateauditStatus/{id}/{auditStatus}")
    public Result updateAuditStatus(@PathVariable Long id,@PathVariable Integer auditStatus){
        productService.updateAuditStatus(id,auditStatus);
        return Result.success(null,"操作成功");
    }
    @GetMapping("/updateStatus/{id}/{status}")
    public Result updateStatus(@PathVariable Long id,@PathVariable Integer status){
        productService.updateStatus(id,status);
        return Result.success(null,"操作成功");
    }

}
