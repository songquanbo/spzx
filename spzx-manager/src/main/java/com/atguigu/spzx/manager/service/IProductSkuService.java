package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.entity.product.ProductSku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品sku 服务类
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
public interface IProductSkuService extends IService<ProductSku> {

}
