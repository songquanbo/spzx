package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.product.ProductSpec;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 商品规格 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-03-10
 */
public interface ProductSpecMapper extends BaseMapper<ProductSpec> {

    void updateProSpec(ProductSpec productSpec);
     @Update("update db_spzx.product_spec set is_deleted=1 where id=#{id}")
    void deleteProSpec(Long id);
}
