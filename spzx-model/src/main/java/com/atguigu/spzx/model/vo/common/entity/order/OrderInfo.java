package com.atguigu.spzx.model.vo.common.entity.order;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 订单
 * </p>
 *
 * @author author
 * @since 2024-03-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("order_info")
public class OrderInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 会员_id
     */
    private Long userId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 使用的优惠券
     */
    private Long couponId;

    /**
     * 订单总额
     */
    private BigDecimal totalAmount;

    /**
     * 优惠券
     */
    private BigDecimal couponAmount;

    /**
     * 原价金额
     */
    private BigDecimal originalTotalAmount;

    /**
     * 运费
     */
    private BigDecimal feightFee;

    /**
     * 支付方式【1->微信 2->支付宝】
     */
    private Integer payType;

    /**
     * 订单状态【0->待付款；1->待发货；2->已发货；3->待用户收货，已完成；-1->已取消】
     */
    private Integer orderStatus;

    /**
     * 收货人姓名
     */
    private String receiverName;

    /**
     * 收货人电话
     */
    private String receiverPhone;

    /**
     * 收货人地址标签
     */
    private String receiverTagName;

    /**
     * 省份/直辖市
     */
    private Long receiverProvince;

    /**
     * 城市
     */
    private Long receiverCity;

    /**
     * 区
     */
    private Long receiverDistrict;

    /**
     * 详细地址
     */
    private String receiverAddress;

    /**
     * 支付时间
     */
    private LocalDateTime paymentTime;

    /**
     * 发货时间
     */
    private LocalDateTime deliveryTime;

    /**
     * 确认收货时间
     */
    private LocalDateTime receiveTime;

    /**
     * 订单备注
     */
    private String remark;

    /**
     * 取消订单时间
     */
    private LocalDateTime cancelTime;

    /**
     * 取消订单原因
     */
    private String cancelReason;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 删除标记（0:不可用 1:可用）
     */
    private Integer isDeleted;


}
