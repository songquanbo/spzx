package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.product.ProductSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 商品sku Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */

public interface ProductSkuMapper extends BaseMapper<ProductSku> {

    void save(ProductSku productSku);

    void updateByProductSkuId(ProductSku item);
    @Update("update db_spzx.product_sku set is_deleted=1 where product_id=#{id}")
    void delProductSku(Long id);
}
