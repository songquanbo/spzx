package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.product.ProductDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 商品sku属性表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
public interface ProductDetailsMapper extends BaseMapper<ProductDetails> {

    void save(ProductDetails productDetails);

    void updateByProductDetailsId(ProductDetails productDetails);
    @Update("update db_spzx.product_details set is_deleted=1 where product_id=#{id}")
    void delProductDetails(Long id);
}
