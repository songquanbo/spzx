package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.entity.product.ProductSpec;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品规格 服务类
 * </p>
 *
 * @author author
 * @since 2024-03-10
 */
public interface IProductSpecService extends IService<ProductSpec> {

    PageInfo<ProductSpec> selectByPage(Integer page, Integer limit);

    void add(ProductSpec productSpec);

    void update(ProductSpec productSpec);

    void deleteById(Long id);

    List<ProductSpec> findAll();
}
