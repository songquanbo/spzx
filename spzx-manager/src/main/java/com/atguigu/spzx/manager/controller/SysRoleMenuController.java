package com.atguigu.spzx.manager.controller;


import com.atguigu.spzx.manager.service.ISysRoleMenuService;
import com.atguigu.spzx.model.vo.common.Result;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/admin/system/sysRoleMenu")
public class SysRoleMenuController {

     @Autowired
     private ISysRoleMenuService sysRoleMenuService;

    /**
     * 回显角色已经分配过的菜单接口
     * @param roleId
     * @return
     */
    @GetMapping("/findMenusWithRoleId/{roleId}")
    public Result<Map<String,Object>> findMenusWithRoleId(@PathVariable("roleId") Long roleId){
        Map<String,Object> map=sysRoleMenuService.findMenusWithRoleId(roleId);
        return Result.success(map);
    }

}
