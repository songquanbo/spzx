package com.atguigu.spzx.manager.controller;

import com.atguigu.spzx.manager.service.ISysUserRoleService;
import com.atguigu.spzx.manager.service.ISysUserService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.constraint.Constraint;
import com.atguigu.spzx.model.vo.common.dto.system.AssignRolesDto;
import com.atguigu.spzx.model.vo.common.dto.system.SysUserDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysUser;
import com.atguigu.spzx.model.vo.common.vo.system.OperationMenuVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Tag(name = "用户接口")
@RestController
@RequestMapping("/admin/system/sysUser")
public class SysUserController {

    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ISysUserRoleService sysUserRoleService;

    /**
     * 用户列表查询
     * @param sysUserDto
     * @param pageNum
     * @param pageSize
     * @return
     */
    @PostMapping  ("/findByPage/{pageNum}/{pageSize}")
    @Operation(summary = "角色分页查询",description = "")
    public Result<Page<SysUser>> selectByPage(
             @RequestBody SysUserDto sysUserDto,
            @PathVariable(value = "pageNum") Integer pageNum ,
            @PathVariable(value = "pageSize") Integer pageSize)
    {
        return Result.success(sysUserService.selectUserByPage(pageNum,pageSize,sysUserDto));
    }
    //添加用户
    @PostMapping("/saveSysUser")
    public Result saveSysUser(@RequestBody @Validated(Constraint.Instert.class) SysUser sysUser){
        sysUserService.saveSysUser(sysUser);
        return Result.success(null,"添加成功");
    }
    //修改用户
    @PutMapping("/updateSysUser")
    public Result updateSysUser(@RequestBody @Validated(Constraint.Update.class) SysUser sysUser){
        sysUserService.updateSysUser(sysUser);
        return Result.success(null,"修改成功");
    }
    //删除用户
    @DeleteMapping("/deleteSysUser/{userId}")
    public Result deleteSysUser(@PathVariable(name = "userId") Long id){
        sysUserService.deleteById(id);
        return Result.success(null,"删除成功");
    }
    /**
     * 根据用户id回显用户所拥有的角色
     * @param userId
     * @return
     */
    @GetMapping("/findAllRolesWithUserId/{userId}")
    public Result<Map<String, Object>> showRolesOfUser(@PathVariable("userId") Long userId){
        Map<String, Object> map= sysUserService.showRolesOfUser(userId);
        return Result.success(map);
    }

    @PostMapping("/doAssign")
    public Result doAssign(@RequestBody AssignRolesDto assignRolesDto){
        sysUserRoleService.doAssign(assignRolesDto);
        return Result.success(null,"分配成功");
    }

    @GetMapping("/operationOfMenu")
    public Result<List<OperationMenuVo>> findOperationOfMenus(){
        List<OperationMenuVo> operationMenuVos= sysUserRoleService.findOperationOfMenus();
        return Result.success(operationMenuVos);
    }

}
