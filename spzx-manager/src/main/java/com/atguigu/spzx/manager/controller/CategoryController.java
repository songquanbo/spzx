package com.atguigu.spzx.manager.controller;


import com.atguigu.spzx.manager.service.ICategoryService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.entity.product.Category;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 商品分类 前端控制器
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-13
 */
@RestController
@RequestMapping("/product/category")
public class CategoryController {
   @Autowired
   private ICategoryService categoryService;

    @GetMapping("findCategories/{id}")
    public Result<List<Category>> findCategoriesById(@PathVariable("id") Long id){
        List<Category> categoryList= categoryService.findCategoriesById(id);
        return Result.success(categoryList);
    }
    /*
    * 商品分类导出
    * */
    @GetMapping(value = "/exportData")
    public void exportData(HttpServletResponse response) {
        categoryService.exportData(response);
    }
    /*
    * 导入
    * */
    @PostMapping("importData")
    public Result importData(MultipartFile file) {
        categoryService.importData(file);
        return Result.success();
    }
}
