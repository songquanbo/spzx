package com.atguigu.spzx.manager.controller;

import com.atguigu.spzx.manager.service.IBrandService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.entity.product.Brand;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/product/brand")
@RequiredArgsConstructor
public class BrandController {

    private final IBrandService brandService;

    /**
     * 分页查询品牌
     * @param page
     * @param limit
     * @return
     */
   @GetMapping("{page}/{limit}")
    public Result<PageInfo<Brand>> list(@PathVariable Integer page, @PathVariable Integer limit){
       return Result.success(brandService.selectByPage(page,limit));
    }
    @GetMapping("/findAll")
    public Result<List<Brand>> findAll(){
       return Result.success(brandService.list());
    }
    //添加品牌
    @PostMapping("/addBrand")
    public Result addBrand(@RequestBody Brand brand){
       brandService.saveBrand(brand);
       return Result.success(null,"添加成功");
    }
    @PutMapping("updateBrand")
    public Result updateBrand(@RequestBody Brand brand){
        brandService.updateBrand(brand);
       return Result.success(null,"修改成功");
    }
    @DeleteMapping("deleteBrand/{id}")
    public Result updateBrand(@PathVariable Long id){
        brandService.deleteById(id);
        return Result.success(null,"修改成功");
    }
}
