package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.dto.order.OrderStatisticsDto;
import com.atguigu.spzx.model.vo.common.entity.order.OrderStatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 订单统计 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-03-16
 */
public interface OrderStatisticsMapper extends BaseMapper<OrderStatistics> {

    void addOrderStatistics(OrderStatistics orderStatistics);

    List<OrderStatistics> selectOrderIn(OrderStatisticsDto orderStatisticsDto);
}
