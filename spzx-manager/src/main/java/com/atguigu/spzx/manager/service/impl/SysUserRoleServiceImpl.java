package com.atguigu.spzx.manager.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.atguigu.spzx.manager.mapper.SysUserRoleMapper;
import com.atguigu.spzx.manager.service.ISysUserRoleService;
import com.atguigu.spzx.model.vo.common.dto.system.AssignRolesDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysMenu;
import com.atguigu.spzx.model.vo.common.entity.system.SysUserRole;
import com.atguigu.spzx.model.vo.common.vo.system.OperationMenuVo;
import com.atguigu.util.RegularizationMenuUtil;
import com.atguigu.util.ThreadLocalUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 用户角色 服务实现类
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-11
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    
    
    /**
     * 根据用户id查询用户所拥有的角色
     * @param userId
     * @return
     */
    public List<Long> findSysRoleByUserId(Long userId) {
        return sysUserRoleMapper.selectRoleByUserId(userId);
    }

    /**
     * 根据userId为用户分配角色
     * @param assignRolesDto
     */
    @Transactional
    public void doAssign(AssignRolesDto assignRolesDto) {
        Long userId = assignRolesDto.getUserId();
        //删除原来的角色
        sysUserRoleMapper.delete(new LambdaQueryWrapper<SysUserRole>()
                .eq(userId!=null,SysUserRole::getUserId,userId));
        List<Long> roleIds = assignRolesDto.getRoleIds();
        for (Long roleId : roleIds) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setRoleId(roleId);
            sysUserRole.setUserId(userId);
            sysUserRole.setIsDeleted(0);
            sysUserRole.setCreateTime(LocalDateTime.now());
            sysUserRole.setUpdateTime(LocalDateTime.now());
            save(sysUserRole);
        }
    }

    /**
     * 查询当前用户所具有的操作权限菜单
     * @return
     */
    public List<OperationMenuVo> findOperationOfMenus() {
        //获取去当前用户id
        Long userId = ThreadLocalUtil.get().getId();
        //获取当前用户具有的操作菜单
        List<SysMenu> sysMenuList= sysUserRoleMapper.findOperationOfMenus(userId);
        //菜单节点树形化
        List<SysMenu> regularMenu = RegularizationMenuUtil.getRegularMenu(sysMenuList);
        return buildMenus(regularMenu);
    }
    // 将List<SysMenu>对象转换成List<SysMenuVo>对象
    private List<OperationMenuVo> buildMenus(List<SysMenu> menus) {

        List<OperationMenuVo> sysMenuVoList = new ArrayList<>();
        for (SysMenu sysMenu : menus) {
            OperationMenuVo operationMenuVo = new OperationMenuVo();
            operationMenuVo.setTitle(sysMenu.getTitle());
            operationMenuVo.setName(sysMenu.getComponent());
            List<SysMenu> children = sysMenu.getChildren();
            if (!CollectionUtil.isEmpty(children)) {
                operationMenuVo.setChildren(buildMenus(children));
            }
            sysMenuVoList.add(operationMenuVo);
        }
        return sysMenuVoList;
    }

}
