package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.entity.order.OrderStatistics;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单统计 服务类
 * </p>
 *
 * @author author
 * @since 2024-03-16
 */
public interface IOrderStatisticsService extends IService<OrderStatistics> {


    void addOrderStatistics(OrderStatistics orderStatistics);
}
