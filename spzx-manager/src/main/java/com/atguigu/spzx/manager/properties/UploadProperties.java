package com.atguigu.spzx.manager.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties("spzx.minio")
@Component
public class UploadProperties  {
   private String endpointUrl;
   private String assceeKey;
   private String secretKey;
   private String bucketName;
}
