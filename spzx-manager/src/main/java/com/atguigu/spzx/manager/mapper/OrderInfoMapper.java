package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.order.OrderInfo;
import com.atguigu.spzx.model.vo.common.entity.order.OrderStatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-03-16
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

    OrderStatistics selectYesOrderData(String createTime);
}
