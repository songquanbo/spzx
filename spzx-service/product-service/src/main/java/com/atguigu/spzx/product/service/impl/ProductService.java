package com.atguigu.spzx.product.service.impl;

import com.atguigu.spzx.model.vo.common.entity.product.ProductSku;
import com.atguigu.spzx.product.mapper.ProductMapper;
import com.atguigu.spzx.product.service.IProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService implements IProductService {

    private final ProductMapper productMapper;
    /**
     * 查询畅销商品
     * @return
     */
    public List<ProductSku> findSellingProducts() {
        return productMapper.findSellingProducts();
    }
}
