package com.atguigu.spzx.product.mapper;

import com.atguigu.spzx.model.vo.common.entity.product.ProductSku;

import java.util.List;

public interface ProductMapper {
    List<ProductSku> findSellingProducts();
}
