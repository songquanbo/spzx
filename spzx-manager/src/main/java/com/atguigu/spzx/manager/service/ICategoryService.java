package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.entity.product.Category;
import com.baomidou.mybatisplus.extension.service.IService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 商品分类 服务类
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-13
 */
public interface ICategoryService extends IService<Category> {

    List<Category> findCategoriesById(Long id);

    void exportData(HttpServletResponse response);

    void importData(MultipartFile file);
}
