package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.entity.product.Brand;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 * 分类品牌 服务类
 * </p>
 *
 * @author author
 * @since 2024-02-28
 */
public interface IBrandService extends IService<Brand> {

    PageInfo<Brand> selectByPage(Integer page, Integer limit);

    void saveBrand(Brand brand);

    void updateBrand(Brand brand);

    void deleteById(Long id);
}
