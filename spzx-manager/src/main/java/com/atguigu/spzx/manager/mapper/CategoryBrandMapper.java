package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.dto.product.CategoryBrandDto;
import com.atguigu.spzx.model.vo.common.entity.product.Brand;
import com.atguigu.spzx.model.vo.common.entity.product.CategoryBrand;
import com.atguigu.spzx.model.vo.common.vo.product.CategoryBrandVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 分类品牌 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-29
 */
public interface CategoryBrandMapper extends BaseMapper<CategoryBrand> {

    List<CategoryBrandVo> page(Page<CategoryBrand> categoryBrandPage, @Param("categoryBrandDto") CategoryBrandDto categoryBrandDto);

    void updateCategoryBrand(CategoryBrand categoryBrand);

    List<Brand> selectBrandByCategoryId(Long categoryId);
}
