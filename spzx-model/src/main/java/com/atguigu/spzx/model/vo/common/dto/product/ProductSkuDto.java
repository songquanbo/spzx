package com.atguigu.spzx.model.vo.common.dto.product;

import lombok.Data;

/**
 * <p>
 * 商品sku
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
@Data
public class ProductSkuDto {
   //关键字
    private String keyword;
   //品牌ID
    private Long brandId;
    //一级分类ID
    private Long category1Id;
    //二级分类ID
    private Long category2Id;
    //三级分类ID
    private Long category3Id;
    //排序（销量:1 价格升序:2 价格降序:3）
    private Integer order;
}
