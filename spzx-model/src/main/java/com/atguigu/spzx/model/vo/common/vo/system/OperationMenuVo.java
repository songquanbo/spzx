package com.atguigu.spzx.model.vo.common.vo.system;

import lombok.Data;

import java.util.List;

@Data
public class OperationMenuVo {
    private String title;
    private String name;
    private List<OperationMenuVo> children;
}
