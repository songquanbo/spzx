package com.atguigu.spzx.constants;

public class ExceptionConstants {

    public static final String USERNAME_IS_NOT_EXISTS="用户名不存在";
    public static final String PASSWORD_ERROR="密码错误";
    public static final String CAPTCHA_ERROR="验证码不正确";

}
