package com.atguigu.spzx.manager.service;


import com.atguigu.spzx.model.vo.common.entity.system.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 角色菜单 服务类
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-12
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    Map<String, Object> findMenusWithRoleId(Long userId);
}
