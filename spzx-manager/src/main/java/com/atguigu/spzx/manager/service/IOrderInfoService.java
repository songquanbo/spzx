package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.dto.order.OrderStatisticsDto;
import com.atguigu.spzx.model.vo.common.entity.order.OrderInfo;
import com.atguigu.spzx.model.vo.common.entity.order.OrderStatistics;
import com.atguigu.spzx.model.vo.common.vo.order.OrderStatisticsVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author author
 * @since 2024-03-16
 */
public interface IOrderInfoService extends IService<OrderInfo> {

    OrderStatistics selectYesOrderData();

    OrderStatisticsVo getOrderStatisticsData(OrderStatisticsDto orderStatisticsDto);
}
