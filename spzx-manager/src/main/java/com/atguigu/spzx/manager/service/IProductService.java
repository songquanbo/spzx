package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.dto.product.ProductDto;
import com.atguigu.spzx.model.vo.common.dto.product.ProductSkuDto;
import com.atguigu.spzx.model.vo.common.entity.product.Product;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import com.atguigu.spzx.model.vo.common.vo.product.ProductVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品 服务类
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
public interface IProductService extends IService<Product> {

    PageInfo<ProductVo> listPage(Integer page, Integer limit, ProductSkuDto productSkuDto);

    void save(ProductDto productDto);

    ProductDto getByProductId(Long id);

    void updateProductById(ProductDto productDto);

    void delProductById(Long id);

    void updateAuditStatus(Long id, Integer auditStatus);

    void updateStatus(Long id, Integer status);
}
