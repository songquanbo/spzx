package com.atguigu.spzx.model.vo.common.dto.system;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;


    /*
    * 验证码
    * */
    private String captcha;


    /*
    *验证码key
    * */

    private String codeKey;

}
