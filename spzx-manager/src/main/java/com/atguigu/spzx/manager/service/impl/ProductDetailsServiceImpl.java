package com.atguigu.spzx.manager.service.impl;

import com.atguigu.spzx.manager.mapper.ProductDetailsMapper;
import com.atguigu.spzx.manager.service.IProductDetailsService;
import com.atguigu.spzx.model.vo.common.entity.product.ProductDetails;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品sku属性表 服务实现类
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
@Service
public class ProductDetailsServiceImpl extends ServiceImpl<ProductDetailsMapper, ProductDetails> implements IProductDetailsService {

}
