package com.atguigu.spzx.product.service.impl;

import com.atguigu.spzx.model.vo.common.entity.product.Category;
import com.atguigu.spzx.product.mapper.CategoryMapper;
import com.atguigu.spzx.product.service.ICategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService implements ICategoryService {
   private final CategoryMapper categoryMapper;
    /**
     * 查询一级分类
     * @return
     */
    public List<Category> findOneCategorys() {
        return categoryMapper.findOneCategorys();
    }
}
