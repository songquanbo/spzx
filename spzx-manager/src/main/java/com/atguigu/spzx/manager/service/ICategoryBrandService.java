package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.dto.product.CategoryBrandDto;
import com.atguigu.spzx.model.vo.common.entity.product.Brand;
import com.atguigu.spzx.model.vo.common.entity.product.CategoryBrand;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import com.atguigu.spzx.model.vo.common.vo.product.CategoryBrandVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 分类品牌 服务类
 * </p>
 *
 * @author author
 * @since 2024-02-29
 */
public interface ICategoryBrandService extends IService<CategoryBrand> {

    PageInfo<CategoryBrandVo> selectByPage(CategoryBrandDto categoryBrandDto, Long page, Long limit);

    void add(CategoryBrand categoryBrand);

    void update(CategoryBrand categoryBrand);

    List<Brand> getBrandByCategoryId(Long categoryId);
}
