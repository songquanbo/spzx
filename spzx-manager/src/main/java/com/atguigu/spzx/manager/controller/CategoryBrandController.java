package com.atguigu.spzx.manager.controller;

import com.atguigu.spzx.manager.service.ICategoryBrandService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.dto.product.CategoryBrandDto;
import com.atguigu.spzx.model.vo.common.entity.product.Brand;
import com.atguigu.spzx.model.vo.common.entity.product.CategoryBrand;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import com.atguigu.spzx.model.vo.common.vo.product.CategoryBrandVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/product/categoryBrand")
@RestController
@RequiredArgsConstructor
@Tag(name = "分类品牌")
public class CategoryBrandController {

    private final ICategoryBrandService categoryBrandService;

    /**
     * 分页查询分类品牌数据
     * @param categoryBrandDto
     * @param page
     * @param limit
     * @return
     */
    @Operation(summary = "分类品牌分页接口",description = "")
    @GetMapping("/{page}/{limit}")
    public Result<PageInfo<CategoryBrandVo>> brandList
            (CategoryBrandDto categoryBrandDto,
             @PathVariable Long page, @PathVariable Long limit){
       return Result.success(categoryBrandService.selectByPage(categoryBrandDto,page,limit));
    }

    /**
     * 添加分类品牌
     * @param categoryBrand
     * @return
     */
    @PostMapping("/addCategoryBrand")
    public Result add(@RequestBody CategoryBrand categoryBrand){
        categoryBrandService.add(categoryBrand);
        return Result.success();
    }

    /**
     * 修改分类品牌
     * @param categoryBrand
     * @return
     */
    @PutMapping("/updateCategoryBrand")
    public Result update(@RequestBody CategoryBrand categoryBrand){
        categoryBrandService.update(categoryBrand);
        return Result.success();
    }
    @GetMapping("findBrandByCategoryId/{categoryId}")
    public Result<List<Brand>> getBrandByCategoryId(@PathVariable Long categoryId){
        return Result.success(categoryBrandService.getBrandByCategoryId(categoryId));
    }

}
