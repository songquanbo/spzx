package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.entity.system.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-12
 */
public interface ISysMenuService extends IService<SysMenu> {

    void addMenu(SysMenu sysMenu);

    List<SysMenu> findMenus();

    void updateMenu(SysMenu sysMenu);

    void deleteMenuById(Long menuId);
}
