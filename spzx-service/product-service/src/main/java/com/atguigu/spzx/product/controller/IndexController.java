package com.atguigu.spzx.product.controller;

import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.vo.product.IndexVo;
import com.atguigu.spzx.product.service.IIndexService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product/index")
@RequiredArgsConstructor
@Tag(name = "app主页面Api")
public class IndexController {

    private final IIndexService indexService;

     @GetMapping
     @Operation(summary = "主页面加载数据接口")
    public Result<IndexVo> index(){
         return Result.success(indexService.index());
     }

}
