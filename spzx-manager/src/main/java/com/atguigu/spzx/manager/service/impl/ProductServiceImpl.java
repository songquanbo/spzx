package com.atguigu.spzx.manager.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.atguigu.spzx.manager.mapper.ProductDetailsMapper;
import com.atguigu.spzx.manager.mapper.ProductMapper;
import com.atguigu.spzx.manager.mapper.ProductSkuMapper;
import com.atguigu.spzx.manager.service.IProductService;
import com.atguigu.spzx.model.vo.common.dto.product.ProductDto;
import com.atguigu.spzx.model.vo.common.dto.product.ProductSkuDto;
import com.atguigu.spzx.model.vo.common.entity.product.Product;
import com.atguigu.spzx.model.vo.common.entity.product.ProductDetails;
import com.atguigu.spzx.model.vo.common.entity.product.ProductSku;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import com.atguigu.spzx.model.vo.common.vo.product.ProductVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 商品 服务实现类
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
@Service
@RequiredArgsConstructor
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    private final ProductMapper productMapper;
    private final ProductSkuMapper productSkuMapper;
    private final ProductDetailsMapper productDetailsMapper;

    /**
     * 分页条件查询商品数据
     * @param page
     * @param limit
     * @param productSkuDto
     * @return
     */
    public PageInfo<ProductVo> listPage(Integer page, Integer limit, ProductSkuDto productSkuDto) {
        //设置分页参数
        Page<ProductVo> productPage = new Page<>(page, limit);
        //执行sql,返回list数据
        List<ProductVo> productList = productMapper.page(productPage,productSkuDto);
        //创建结果集对象
        PageInfo<ProductVo> productPageInfo = new PageInfo<>();
        //设置list
        productPageInfo.setRecords(productList);
        //设置total
        productPageInfo.setTotal(productPage.getTotal());
        //设置size
        productPageInfo.setSize(productPage.getSize());
        //设置current页数
        productPageInfo.setCurrent(productPage.getCurrent());
        return productPageInfo;
    }

    /**
     * 添加商品信息
     * @param productDto
     */
    @Transactional
    public void save(ProductDto productDto) {
        //1.保存product基本信息
        //1.1创建product对象
        Product product = new Product();
        //1.2将productDto中的数据copy到product中
        BeanUtil.copyProperties(productDto,product);
        //1.3保存product信息
        productMapper.save(product);
        //2.保存product的sku列表信息
        //2.1获取productId
        Long productId = product.getId();
        //2.2获取productDto中sku信息
        List<ProductSku> productSkuList = productDto.getProductSkuList();
        int count=0;
        for (ProductSku productSku : productSkuList) {
            //设置sku_code
            productSku.setSkuCode(productId+"_"+count++);
            //设置skuName
            productSku.setSkuName(productDto.getName()+productSku.getSkuSpec());
            //设置productId
            productSku.setProductId(productId);
            productSkuMapper.save(productSku);
        }
        //3.product_details的详细信息
        ProductDetails productDetails = new ProductDetails();
        //设置productId
        productDetails.setProductId(productId);
        //设置图片路径
        productDetails.setImageUrls(productDto.getDetailsImageUrls());
        //保存数据
        productDetailsMapper.save(productDetails);

    }

    /**
     * 根据productId查询商品数据
     * @param id
     * @return
     */
    public ProductDto getByProductId(Long id) {
        //1.查询商品基本信息
        Product product = productMapper.selectByProductId(id);
        //2.查询商品sku信息
        List<ProductSku> productSkus = productSkuMapper
                .selectList(new LambdaQueryWrapper<ProductSku>()
                        .eq(id != null, ProductSku::getProductId, id));
        //3.查询商品详细信息
        ProductDetails productDetails = productDetailsMapper
                .selectOne(new LambdaQueryWrapper<ProductDetails>()
                        .eq(id != null, ProductDetails::getProductId, id));
        //4.填充属性
        ProductDto productDto = new ProductDto();
        //4.1基本信息
        BeanUtil.copyProperties(product,productDto);
        //4.2sku信息
        productDto.setProductSkuList(productSkus);
        //4.3详细信息
        productDto.setDetailsImageUrls(productDetails.getImageUrls());
        //5.返回数据
        return productDto;
    }

    /**
     * 根据id修改商品信息
     * @param productDto
     */
    @Transactional
    public void updateProductById(ProductDto productDto) {
        //1.修改基本信息
        Product product = new Product();
        BeanUtil.copyProperties(productDto,product);
        productMapper.updateByProductId(product);
        //2.修改sku信息
        //2.1获取商品名
        String productName = productDto.getName();
        productDto.getProductSkuList().forEach(item->{
            item.setSkuName(productName+" "+item.getSkuSpec());
            productSkuMapper.updateByProductSkuId(item);
        });
        //3.修改details信息
        ProductDetails productDetails = new ProductDetails();
        productDetails.setProductId(productDto.getId());
        productDetails.setImageUrls(productDto.getDetailsImageUrls());
        productDetailsMapper.updateByProductDetailsId(productDetails);
    }

    /**
     * 根据id删除商品数据
     * @param id
     */
    @Transactional
    public void delProductById(Long id) {
        //1.商品基本信息del
        productMapper.delProduct(id);
        //2.sku信息del
        productSkuMapper.delProductSku(id);
        //3.商品详细信息del
        productDetailsMapper.delProductDetails(id);
    }

    /**
     * 根据id审批商品
     * @param id
     * @param auditStatus
     */
    public void updateAuditStatus(Long id, Integer auditStatus) {
        //1.修改商品基本信息状态
        Product product = new Product();
        product.setId(id);
        if(auditStatus == 1) {
            product.setAuditStatus(1);
            product.setAuditMessage("审批通过");
        } else {
            product.setAuditStatus(-1);
            product.setAuditMessage("审批不通过");
        }
        productMapper.updateByProductId(product);
    }

    /**
     * 根据id修改商品上下架状态
     * @param id
     * @param status
     */
    public void updateStatus(Long id, Integer status) {
        Product product = new Product();
        product.setId(id);
        if(status == 1) {
            product.setStatus(1);
        } else {
            product.setStatus(-1);
        }
        productMapper.updateByProductId(product);
    }
}
