package com.atguigu.spzx.product.service.impl;

import com.atguigu.spzx.model.vo.common.entity.product.Category;
import com.atguigu.spzx.model.vo.common.entity.product.ProductSku;
import com.atguigu.spzx.model.vo.common.vo.product.IndexVo;
import com.atguigu.spzx.product.service.ICategoryService;
import com.atguigu.spzx.product.service.IIndexService;
import com.atguigu.spzx.product.service.IProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class IndexService implements IIndexService {

    private final ICategoryService categoryService;
    private final IProductService productService;


    /**
     * 获取主页面数据,返回
     * @return
     */
    public IndexVo index() {
        //1.查询一级分类
       List<Category> categoryList= categoryService.findOneCategorys();
        //2.查询畅销商品
        List<ProductSku> productSkuList = productService.findSellingProducts();
        //3.封装结果集,并返回
        IndexVo indexVo = new IndexVo();
        indexVo.setCategoryList(categoryList);
        indexVo.setProductSkuList(productSkuList);
        return indexVo;
    }
}
