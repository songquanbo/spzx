package com.atguigu.spzx.manager.controller;

import com.atguigu.spzx.manager.service.ISysMenuService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.entity.system.SysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/system/sysMenu")
public class SysMenuController {
     @Autowired
     private ISysMenuService sysMenuService;

     @GetMapping("/findMenus")
     public Result<List<SysMenu> > findMenus(){
         List<SysMenu> sysMenuList=sysMenuService.findMenus();
         return Result.success(sysMenuList);
     }

    @PostMapping("/addMenu")
    public Result addMenu(@RequestBody SysMenu sysMenu){
        sysMenuService.addMenu(sysMenu);
        return Result.success(null,"添加成功");
    }

    @PutMapping("/updateMenu")
    public Result updateMenu(@RequestBody SysMenu sysMenu){
          sysMenuService.updateMenu(sysMenu);
         return Result.success(null,"修改成功");
    }

    @DeleteMapping("/deleteMenu/{menuId}")
    public Result deleteMenu(@PathVariable("menuId") Long menuId){
        sysMenuService.deleteMenuById(menuId);
        return Result.success(null,"删除成功");
    }


}
