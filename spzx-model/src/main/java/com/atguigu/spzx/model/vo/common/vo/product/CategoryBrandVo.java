package com.atguigu.spzx.model.vo.common.vo.product;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 分类品牌Vo
 * </p>
 *
 * @author author
 * @since 2024-02-29
 */
@Data
public class CategoryBrandVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long brandId;

    private Long categoryId;

    private String brandName;

    private String categoryName;

    private String logo;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Integer isDeleted;
}
