package com.atguigu.spzx.manager.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.atguigu.spzx.exception.BaseException;
import com.atguigu.spzx.manager.mapper.SysRoleMapper;
import com.atguigu.spzx.manager.service.ISysRoleService;
import com.atguigu.spzx.manager.service.ISysUserRoleService;
import com.atguigu.spzx.model.vo.common.dto.system.AssignMenuDto;
import com.atguigu.spzx.model.vo.common.dto.system.SysRoleDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysRole;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author 宋哥
 * @since 2023-12-28
 */
@Service
@Slf4j
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Autowired
    private SysRoleMapper roleMapper;
    @Override
    public Page<SysRole> page(Integer pageNum, Integer pageSize, SysRoleDto sysRoleDto) {
        Page<SysRole> page = new Page<>(pageNum, pageSize);
        return page(page, new LambdaQueryWrapper<SysRole>()
                .like(StrUtil.isNotBlank(
                        sysRoleDto.getRoleName())
                        , SysRole::getRoleName
                        , sysRoleDto.getRoleName()).eq(SysRole::getIsDeleted,0));
    }

    /**
     * 添加角色
     * @param sysRole
     */
    public void addRole(SysRole sysRole) {
        try {
            roleMapper.insert(sysRole);
        } catch (Exception e) {
            log.info("添加失败");
            throw new BaseException("添加失败");
        }
    }

    /**
     * 根据id查询角色信息
     * @param id
     * @return
     */
    public SysRole selectById(Long id) {
        return roleMapper.selectById(id);
    }

    /**
     * 修改角色数据
     * @param sysRole
     */
    public void updateRole(SysRole sysRole) {
        int i = roleMapper.updateById(sysRole);
        if(i<=0){
            throw new BaseException("修改失败");
        }
    }

    /**
     * 根据id逻辑删除角色
     * @param id
     */
    public void deleteRole(Long id) {
        SysRole sysRole = new SysRole();
        sysRole.setId(id);
        sysRole.setIsDeleted(1);
        //修改
        roleMapper.updateById(sysRole);
    }
    /**
     * 查询所有角色
     * @return
     */
    public List<SysRole> findAllRoles(){
        return list();
    }

    /**
     * 为角色分配菜单
     * @param assignMenuDto
     */
    public void doAssignMenu(AssignMenuDto assignMenuDto) {
        // 根据角色的id删除其所对应的菜单数据
        roleMapper.deleteByRoleId(assignMenuDto.getRoleId());

        // 获取菜单的id
        List<Map<String, Integer>> menuInfo = assignMenuDto.getMenuIdList();
        if(CollectionUtil.isNotEmpty(menuInfo)) {
            roleMapper.doAssign(assignMenuDto);
        }
    }

}
