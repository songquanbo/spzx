package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.dto.system.AssignMenuDto;
import com.atguigu.spzx.model.vo.common.dto.system.SysRoleDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author 宋哥
 * @since 2023-12-28
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<SysRole> page(@Param("sysRoleDto") SysRoleDto sysRoleDto, IPage<SysRole> page);
     @Delete("delete  from sys_role_menu where role_id=#{roleId}")
    void deleteByRoleId(Long roleId);

    void doAssign(@Param("assignMenuDto") AssignMenuDto assignMenuDto);
}
