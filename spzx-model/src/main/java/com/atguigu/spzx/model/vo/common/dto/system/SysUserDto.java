package com.atguigu.spzx.model.vo.common.dto.system;

import lombok.Data;

@Data
public class SysUserDto {
    /**
     * 关键字
     */
    private String keyword ;

    /*
    * 创建时间开始
    * */
    private String createTimeBegin;

    /*
    * 创建时间结束
    * */
    private String createTimeEnd;
}
