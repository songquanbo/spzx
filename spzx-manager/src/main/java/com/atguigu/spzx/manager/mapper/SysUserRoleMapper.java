package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.system.SysMenu;
import com.atguigu.spzx.model.vo.common.entity.system.SysRole;
import com.atguigu.spzx.model.vo.common.entity.system.SysUserRole;
import com.atguigu.spzx.model.vo.common.vo.system.OperationMenuVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 用户角色 Mapper 接口
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-11
 */

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
     @Select("select sys_user_role.role_id from sys_user_role where sys_user_role.user_id=#{userId} ")
    List<Long> selectRoleByUserId(Long userId);

    List<SysMenu> findOperationOfMenus(Long userId);
}
