package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.dto.system.AssignMenuDto;
import com.atguigu.spzx.model.vo.common.dto.system.SysRoleDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysRole;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author 宋哥
 * @since 2023-12-28
 */
public interface ISysRoleService extends IService<SysRole> {


    Page<SysRole> page(Integer pageNum, Integer pageSize, SysRoleDto sysRoleDto);

    void addRole(SysRole sysRole);

    SysRole selectById(Long id);

    void updateRole(SysRole sysRole);

    void deleteRole(Long id);

    List<SysRole> findAllRoles();


    void doAssignMenu(AssignMenuDto assignMenuDto);
}
