package com.atguigu.spzx.manager.service;

import com.atguigu.spzx.model.vo.common.entity.product.ProductUnit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品单位 服务类
 * </p>
 *
 * @author author
 * @since 2024-03-14
 */
public interface IProductUnitService extends IService<ProductUnit> {

}
