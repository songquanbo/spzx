package com.atguigu.spzx.product.service;

import com.atguigu.spzx.model.vo.common.entity.product.Category;

import java.util.List;

public interface ICategoryService {
    List<Category> findOneCategorys();
}
