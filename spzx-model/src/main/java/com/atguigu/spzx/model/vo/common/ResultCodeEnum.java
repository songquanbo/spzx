package com.atguigu.spzx.model.vo.common;

import lombok.Getter;

@Getter // 提供获取属性值的getter方法
public enum ResultCodeEnum {

    SUCCESS( "操作成功") ,
    LOGIN_ERROR( "用户名或者密码错误"),
    VALIDATECODE_ERROR( "验证码错误") ,
    LOGIN_AUTH( "用户未登录"),
    USER_NAME_IS_EXISTS( "用户名已经存在"),
    SYSTEM_ERROR( "您的网络有问题请稍后重试"),
    NODE_ERROR(  "该节点下有子节点，不可以删除"),
    DATA_ERROR( "数据异常"),
    ACCOUNT_STOP(  "账号已停用"),

    STOCK_LESS(  "库存不足"),

    ;

    private String message ;    // 响应消息

    private ResultCodeEnum(String message) {
        this.message = message ;
    }

}
