package com.atguigu.spzx.manager.controller;


import com.atguigu.spzx.manager.service.ISysUserService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.dto.system.LoginDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysUser;
import com.atguigu.spzx.model.vo.common.vo.system.CaptchaVo;
import com.atguigu.spzx.model.vo.common.vo.system.LoginVo;
import com.atguigu.util.ThreadLocalUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author 宋哥
 * @since 2023-12-25
 */
@Tag(name = "账户管理接口")
@RestController
@RequestMapping("/admin/system/index")
public class SysAccountController {

     @Autowired
     private ISysUserService sysUserService;

     @PostMapping("/login")
     @ApiOperation("登录接口")
     public Result<LoginVo> Login(@RequestBody LoginDto loginDto){
         return Result.success(sysUserService.login(loginDto),"登陆成功");
     }
      /*
      * 验证码的key
      * 图片验证码对应的字符串数据
      * */
     @Operation(description = "生成验证码接口")
     @GetMapping("/generateValidateCode")
     public Result<CaptchaVo> generateValidateCode(){
         return Result.success(sysUserService.generateValidateCode(),"验证码生成");
     }
    @Operation(description = "查询用户接口")
     @GetMapping("/getUserInfo")
     public Result<SysUser> getUserInfo(){
         SysUser threadLocal = ThreadLocalUtil.get();
         threadLocal.setPassword("");
         return Result.success(threadLocal);
     }
    /*
     * 用户登出
     * */
    @Operation(description = "登出接口")
    @GetMapping("/logout")
    public Result logout(@RequestHeader(name = "token") String token){
        sysUserService.logout(token);
        return Result.success();
    }

}
