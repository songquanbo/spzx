package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.system.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-12
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {
   @Select("select * from sys_menu")
    List<SysMenu> finndAllMenus();
}
