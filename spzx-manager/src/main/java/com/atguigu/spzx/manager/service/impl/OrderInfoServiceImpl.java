package com.atguigu.spzx.manager.service.impl;

import cn.hutool.core.date.DateUtil;
import com.atguigu.spzx.manager.mapper.OrderInfoMapper;
import com.atguigu.spzx.manager.mapper.OrderStatisticsMapper;
import com.atguigu.spzx.manager.service.IOrderInfoService;
import com.atguigu.spzx.model.vo.common.dto.order.OrderStatisticsDto;
import com.atguigu.spzx.model.vo.common.entity.order.OrderInfo;
import com.atguigu.spzx.model.vo.common.entity.order.OrderStatistics;
import com.atguigu.spzx.model.vo.common.vo.order.OrderStatisticsVo;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author author
 * @since 2024-03-16
 */
@Service
@RequiredArgsConstructor
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements IOrderInfoService {

    private final OrderInfoMapper orderInfoMapper;
    private final OrderStatisticsMapper orderStatisticsMapper;
    /**
     * 查询昨日订单数据
     * @return
     */
    public OrderStatistics selectYesOrderData() {
        //前天的日期
        String createTime = DateUtil.offsetDay(new Date(), -1).toString(new SimpleDateFormat("yyyy-MM-dd"));
        return orderInfoMapper.selectYesOrderData(createTime);
    }

    /**
     * 统计订单数据
     * @param orderStatisticsDto
     * @return
     */
    public OrderStatisticsVo getOrderStatisticsData(OrderStatisticsDto orderStatisticsDto) {
        //1.查询订单数据在所差范围内的
       List<OrderStatistics> orderStatisticsList= orderStatisticsMapper.selectOrderIn(orderStatisticsDto);
       //2.循环遍历获得每日总金额,以及日期集
        List<BigDecimal> bigDecimals = orderStatisticsList
                .stream()
                .map(OrderStatistics::getTotalAmount)
                .collect(Collectors.toList());
        //日期列表
        List<String> dateList = orderStatisticsList
                .stream()
                .map(orderStatistics -> DateUtil.format(orderStatistics.getOrderDate().atStartOfDay(), "yyyy-MM-dd"))
                .collect(Collectors.toList());

        //3.封装结果集
        OrderStatisticsVo orderStatisticsVo = new OrderStatisticsVo();
        orderStatisticsVo.setAmountList(bigDecimals);
        orderStatisticsVo.setDateList(dateList);
        return orderStatisticsVo;
    }
}
