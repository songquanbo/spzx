package com.atguigu.spzx.product.mapper;

import com.atguigu.spzx.model.vo.common.entity.product.Category;

import java.util.List;

public interface CategoryMapper {
    List<Category> findOneCategorys();
}
