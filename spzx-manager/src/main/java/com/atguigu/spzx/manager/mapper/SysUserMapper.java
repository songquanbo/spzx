package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.dto.system.SysUserDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 宋哥
 * @since 2023-12-25
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> page(Page<SysUser> page, SysUserDto sysUserDto);

    int updateSysUser(@Param("sysUser") SysUser sysUser);
}
