package com.atguigu.spzx.model.vo.common.page;

import lombok.Data;

import java.util.List;

@Data
public class PageInfo<T> {
    //数据data
    public List<T> records;
    //数据查询总数
    public long total;
    //该分页下的数据数
    public long size;
    //当前属于第几页
    public long current;
}
