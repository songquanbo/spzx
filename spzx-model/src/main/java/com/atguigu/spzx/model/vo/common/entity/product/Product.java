package com.atguigu.spzx.model.vo.common.entity.product;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 商品
 * </p>
 *
 * @author author
 * @since 2024-03-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 品牌ID
     */
    private Long brandId;

    /**
     * 一级分类id
     */
    private Long category1Id;

    /**
     * 二级分类id
     */
    private Long category2Id;

    /**
     * 三级分类id
     */
    private Long category3Id;

    /**
     * 计量单位
     */
    private String unitName;

    /**
     * 轮播图
     */
    private String sliderUrls;

    /**
     * 商品规格json
     */
    private String specValue;

    /**
     * 线上状态：0-初始值，1-上架，-1-自主下架
     */
    private Integer status;

    /**
     * 审核状态：0-初始值，1-通过，-1-未通过
     */
    private Integer auditStatus;

    /**
     * 审核信息
     */
    private String auditMessage;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 删除标记（0:不可用 1:可用）
     */
    private Integer isDeleted;


}
