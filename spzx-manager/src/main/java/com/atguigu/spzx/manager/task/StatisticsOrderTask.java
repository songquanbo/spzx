package com.atguigu.spzx.manager.task;

import com.atguigu.spzx.manager.service.IOrderInfoService;
import com.atguigu.spzx.manager.service.IOrderStatisticsService;
import com.atguigu.spzx.model.vo.common.entity.order.OrderStatistics;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class StatisticsOrderTask {
      //用于查询生成昨日订单信息
      private final IOrderInfoService orderInfoService;
      //将昨日生成的订单数据添加到statistics表中，便于统计
      private final IOrderStatisticsService orderStatisticsService;
    @Scheduled(cron = "0 0 2 * * ?")
    public void todayOrderStatistics(){
        log.info("定时任务统计订单");
       OrderStatistics orderStatistics = orderInfoService.selectYesOrderData();
        orderStatisticsService.addOrderStatistics(orderStatistics);
    }
}
