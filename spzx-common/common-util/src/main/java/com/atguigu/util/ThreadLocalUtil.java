package com.atguigu.util;

import com.atguigu.spzx.model.vo.common.entity.system.SysUser;

public class ThreadLocalUtil {

    private static final ThreadLocal<SysUser> THREAD_LOCAL=new ThreadLocal<>();

    /*
    * 设置添加threadlocal信息
    * */
    public static void set(SysUser sysUser){
        THREAD_LOCAL.set(sysUser);
    }
    /*
     * 设置获取threadlocal信息
     * */
    public static SysUser get(){
        return THREAD_LOCAL.get();
    }

    /*
     * 销毁threadlocal信息
     * */
    public static void del(){
        THREAD_LOCAL.remove();
    }
}
