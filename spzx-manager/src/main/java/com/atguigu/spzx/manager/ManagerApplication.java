package com.atguigu.spzx.manager;

import com.atguigu.spzx.EnableAutoCommon;
import com.atguigu.spzx.common.log.annotation.EnableLogAspect;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan(basePackages = {"com.atguigu.spzx.manager.mapper"})
@EnableDiscoveryClient
@EnableScheduling//开启定时任务
@EnableLogAspect//开启自定义注解，记录操作日志
@EnableAutoCommon
public class ManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ManagerApplication.class,args);
    }
}
