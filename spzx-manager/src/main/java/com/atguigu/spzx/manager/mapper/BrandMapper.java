package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.product.Brand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 分类品牌 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-28
 */
public interface BrandMapper extends BaseMapper<Brand> {


    void updateBrand(Brand brand);
}
