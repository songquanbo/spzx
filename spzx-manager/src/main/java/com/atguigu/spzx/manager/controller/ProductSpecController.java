package com.atguigu.spzx.manager.controller;


import com.atguigu.spzx.manager.service.IProductSpecService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.entity.product.ProductSpec;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 商品规格 前端控制器
 * </p>
 *
 * @author author
 * @since 2024-03-10
 */
@RestController
@RequestMapping("/admin/product/productSpec")
@RequiredArgsConstructor
public class ProductSpecController {

   private final IProductSpecService productSpecService;
    @GetMapping("/selectByPage/{page}/{limit}")
    public Result<PageInfo<ProductSpec>> page(@PathVariable Integer page, @PathVariable Integer limit){
       return Result.success( productSpecService.selectByPage(page,limit));
    }

    /**
     * 添加规格
     * @param productSpec
     * @return
     */
    @PostMapping("/addProSpec")
    public Result add(@RequestBody ProductSpec productSpec){
        productSpecService.add(productSpec);
        return Result.success();
    }

    /**
     * 修改规格信息
     * @param productSpec
     * @return
     */
    @PutMapping("/updateProSpec")
    public Result update(@RequestBody ProductSpec productSpec){
        productSpecService.update(productSpec);
        return Result.success();
    }
    @DeleteMapping("delProSpec/{id}")
    public Result delete(@PathVariable Long id){
        productSpecService.deleteById( id);
        return Result.success();
    }

    /**
     * 查询所有商品规格信息
     * @return
     */
    @GetMapping("findAll")
    public Result<List<ProductSpec>> findAll(){
        return Result.success(productSpecService.findAll());
    }
}
