package com.atguigu.spzx.manager.constants;

public class RedisConstant {
    public static final String ADMIN_LOGIN="admin:login:";
    public static final String CAPTCHA="random:captcha:";
    public static final Long LOGIN_TTL=30L;
    public static final Long CAPTCHA_TTL=2L;


}
