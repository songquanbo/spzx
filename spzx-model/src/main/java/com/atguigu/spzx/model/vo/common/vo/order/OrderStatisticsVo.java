package com.atguigu.spzx.model.vo.common.vo.order;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderStatisticsVo {
    /**
     * 订单日期集
     */
  private  List<String> dateList;

    /**
     * 订单总金额集
     */
  private List<BigDecimal> amountList;
}
