package com.atguigu.spzx.model.vo.common.vo.product;

import com.atguigu.spzx.model.vo.common.entity.product.Category;
import com.atguigu.spzx.model.vo.common.entity.product.ProductSku;
import lombok.Data;

import java.util.List;

@Data
public class IndexVo {
    /*
     * 所有的一级分类
     * */
    private List<Category> categoryList;

    /*
     * 畅销商品,排名前十
     * */
    private List<ProductSku> productSkuList;
}
