package com.atguigu.spzx.manager.service;

import org.springframework.web.multipart.MultipartFile;

public interface IUploadService {
    String fileUpload(MultipartFile file);
}
