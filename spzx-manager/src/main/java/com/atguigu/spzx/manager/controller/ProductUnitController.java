package com.atguigu.spzx.manager.controller;


import com.atguigu.spzx.manager.service.IProductUnitService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.entity.product.ProductUnit;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 商品单位 前端控制器
 * </p>
 *
 * @author author
 * @since 2024-03-14
 */
@RestController
@RequestMapping("/admin/product/productUnit")
@RequiredArgsConstructor
public class ProductUnitController {

    private final IProductUnitService productUnitService;

    @GetMapping("findAll")
    public Result<List<ProductUnit>> findAll(){
        return Result.success(productUnitService.list());
    }

}
