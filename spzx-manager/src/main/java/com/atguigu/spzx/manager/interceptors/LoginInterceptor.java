package com.atguigu.spzx.manager.interceptors;

import com.alibaba.fastjson2.JSON;
import com.atguigu.spzx.model.vo.common.entity.system.SysUser;
import com.atguigu.util.JwtUtil;
import com.atguigu.util.ThreadLocalUtil;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.concurrent.TimeUnit;

import static com.atguigu.spzx.manager.constants.RedisConstant.ADMIN_LOGIN;
import static com.atguigu.spzx.manager.constants.RedisConstant.LOGIN_TTL;

@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        
         //1.从请求头中获取token
        String token = request.getHeader("token");
        String method = request.getMethod();
        if("OPTIONS".equals(method)){
            return true;
        }
        //2.解析token
        Claims claims =null;
        try {
            claims=JwtUtil.parsePayload(token);
        } catch (Exception e) {
            log.info("token不存在");
            //返回响应状态码,表示未登录
            response.setStatus(401);
            return false;
        }
        //3.判断token是否存在与redis
        Object id = claims.get("id");
        //根据用户id获取redis中存取的token信息
        String userStr = stringRedisTemplate.opsForValue().get(ADMIN_LOGIN +id);
        //判断是否存在
        if(StringUtils.isEmpty(userStr)){
            //redis中不存在token
            response.setStatus(401);
            return false;
        }
        //解析redis中的数据
        SysUser sysUser = JSON.parseObject(userStr, SysUser.class);
        //存在,放入threadlocal
        ThreadLocalUtil.set(sysUser);
        //4.刷新token时间
        stringRedisTemplate.opsForValue().set(ADMIN_LOGIN +id,userStr,LOGIN_TTL, TimeUnit.MINUTES);
        log.info("放行");
        //5.放行
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        SysUser user = ThreadLocalUtil.get();
        //移除threadlocal信息
        ThreadLocalUtil.del();
    }
}
