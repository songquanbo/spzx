package com.atguigu.spzx.model.vo.common.dto.product;

import lombok.Data;

@Data
public class CategoryBrandDto {
    private Long brandId;
    private Long categoryId;

}
