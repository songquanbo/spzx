package com.atguigu.spzx.manager.controller;

import com.atguigu.spzx.common.log.annotation.Log;
import com.atguigu.spzx.common.log.enums.OperatorType;
import com.atguigu.spzx.manager.service.ISysRoleService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.constraint.Constraint;
import com.atguigu.spzx.model.vo.common.dto.system.AssignMenuDto;
import com.atguigu.spzx.model.vo.common.dto.system.SysRoleDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysRole;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/admin/system/sysRole")
@Tag(name = "角色管理接口")
public class SysRoleController {
   @Autowired
   private ISysRoleService sysRoleService;

      @PostMapping("/findByPage/{pageNum}/{pageSize}")
      @Operation(summary = "角色分页查询",description = "")
     public Result<Page<SysRole>> selectByPage(
             @RequestBody SysRoleDto sysRoleDto,
             @PathVariable(value = "pageNum") Integer pageNum ,
             @PathVariable(value = "pageSize") Integer pageSize)
      {
          return Result.success(sysRoleService.page(pageNum,pageSize,sysRoleDto));
      }

    @PostMapping("/saveRole")
    @Operation(summary = "添加角色")
    @Log(title = "品牌管理:添加角色",businessType = 1,operatorType = OperatorType.MANAGE)
    public Result saveRole(@RequestBody SysRole sysRole){
        sysRoleService.addRole(sysRole);
        return Result.success("添加成功");
    }
    @GetMapping("/selectById")
    public Result<SysRole> selectById(@RequestParam(name = "id") Long id){
        return Result.success(sysRoleService.selectById(id));
    }

    @PutMapping("/updateRole")
     public Result updateRole(@RequestBody @Validated(Constraint.Update.class) SysRole sysRole){
        sysRoleService.updateRole(sysRole);
          return Result.success("修改成功");
    }
    @DeleteMapping("/deleteRole/{id}")
        public Result deleteRole(@PathVariable("id") Long id){
        sysRoleService.deleteRole(id);
           return Result.success();
        }
     @PostMapping("/assignMenu")
    public Result assignMenu(@RequestBody AssignMenuDto assignMenuDto){
         sysRoleService.doAssignMenu(assignMenuDto);
         return Result.success();
     }

}
