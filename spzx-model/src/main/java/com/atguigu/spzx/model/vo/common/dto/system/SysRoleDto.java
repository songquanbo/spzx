package com.atguigu.spzx.model.vo.common.dto.system;

import lombok.Data;

@Data
public class SysRoleDto {
    private String roleName;
}
