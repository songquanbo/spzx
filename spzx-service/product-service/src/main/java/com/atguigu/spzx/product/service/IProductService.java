package com.atguigu.spzx.product.service;

import com.atguigu.spzx.model.vo.common.entity.product.ProductSku;

import java.util.List;

public interface IProductService {
    List<ProductSku> findSellingProducts();
}
