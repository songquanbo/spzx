package com.atguigu.spzx.exception;

public class BaseException extends RuntimeException{
    public BaseException(String message) {
        super(message);
    }
}
