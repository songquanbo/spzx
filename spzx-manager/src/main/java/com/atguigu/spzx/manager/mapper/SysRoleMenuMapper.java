package com.atguigu.spzx.manager.mapper;

import com.atguigu.spzx.model.vo.common.entity.system.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 角色菜单 Mapper 接口
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-12
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

    @Select("select menu_id from sys_role_menu where role_id=#{roleId} and is_half=0")
    List<Long> findMenusIdByRoleId(Long roleId);
}
