package com.atguigu.spzx.manager;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import io.minio.errors.MinioException;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class TestUpload {
    public static void main(String[] args) {
        try {
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint("http://192.168.72.129:9000")
                            .credentials("173220105", "173220105")
                            .build();
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("spzx-bucket01").build());
            if (!found) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket("spzx-bucket01").build());
            }
            minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket("spzx-bucket01")
                            .object("asiaphotos-2015.zip")
                            .filename("C:\\Users\\宋全波\\Desktop\\Snipaste_2024-01-05_21-06-27.jpg")
                            .build());

        } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException e) {
            System.out.println("Error occurred: " + e);
            System.out.println("HTTP trace: " + e.getMessage());
        }
    }
}
