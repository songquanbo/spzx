package com.atguigu.spzx.manager.controller;

import com.atguigu.spzx.manager.service.IUploadService;
import com.atguigu.spzx.model.vo.common.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/admin/system/upload")
public class UploadController {

    @Autowired
   private IUploadService uploadService;

    @PostMapping("/fileUpload")
   public Result<String> fileUpload(@RequestParam("file")MultipartFile file){
       String path= uploadService.fileUpload(file);
       return Result.success(path,"上传成功");
   }

}
