package com.atguigu.spzx.manager.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.atguigu.spzx.exception.BaseException;
import com.atguigu.spzx.manager.mapper.BrandMapper;
import com.atguigu.spzx.manager.service.IBrandService;
import com.atguigu.spzx.model.vo.common.entity.product.Brand;
import com.atguigu.spzx.model.vo.common.page.PageInfo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * <p>
 * 分类品牌 服务实现类
 * </p>
 *
 * @author author
 * @since 2024-02-28
 */
@Service
@RequiredArgsConstructor
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements IBrandService {

    public final BrandMapper brandMapper;


    /**
     * 分页查询品牌信息
     * @param page
     * @param limit
     * @return
     */
    public PageInfo<Brand> selectByPage(Integer page, Integer limit) {
        Page<Brand> brandPage = brandMapper.selectPage(new Page<>(page, limit), new LambdaQueryWrapper<>());
        PageInfo<Brand> brandPageInfo = new PageInfo<>();
        BeanUtil.copyProperties(brandPage,brandPageInfo);
        return brandPageInfo;
    }

    /**
     * 添加品牌
     * @param brand
     */
    @Transactional
    public void saveBrand(Brand brand) {
        //1.设置brand各项参数
        brand.setCreateTime(LocalDateTime.now());
        brand.setIsDeleted(0);
        brand.setUpdateTime(LocalDateTime.now());
        //2.添加
        save(brand);
    }

    /**
     * 修改品牌
     */
    @Transactional
    public void updateBrand( Brand brand) {
        brandMapper.updateBrand(brand);
    }

    /**
     * 根据id删除品牌
     * @param id
     */
    @Transactional
    public void deleteById(Long id) {
        int isSuccess = brandMapper.deleteById(id);
        if(isSuccess<1){
            throw new BaseException("删除失败");
        }
    }
}
