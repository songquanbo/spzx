package com.atguigu.spzx.manager.controller;


import com.atguigu.spzx.manager.service.IOrderInfoService;
import com.atguigu.spzx.model.vo.common.Result;
import com.atguigu.spzx.model.vo.common.dto.order.OrderStatisticsDto;
import com.atguigu.spzx.model.vo.common.vo.order.OrderStatisticsVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author author
 * @since 2024-03-16
 */
@RestController
@RequestMapping("/admin/order/orderInfo")
@RequiredArgsConstructor
public class OrderInfoController {
    private final IOrderInfoService orderInfoService ;

    @GetMapping("/getOrderStatisticsData")
    public Result<OrderStatisticsVo> getOrderStatisticsData(OrderStatisticsDto orderStatisticsDto) {
        return Result.success(orderInfoService.getOrderStatisticsData(orderStatisticsDto));
    }
}
