package com.atguigu.util;


import cn.hutool.core.collection.CollectionUtil;
import com.atguigu.spzx.model.vo.common.entity.system.SysMenu;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class RegularizationMenuUtil {

    public static List<SysMenu> getRegularMenu(List<SysMenu> sysMenuList) {
        Map<Long, SysMenu> menuMap = sysMenuList.stream()
                .filter(value -> value.getId() != 0)
                .collect(Collectors.toMap(SysMenu::getId, value -> value));
        //返回的结果集
        ArrayList<SysMenu> menus = new ArrayList<>();
        sysMenuList.stream()
                .filter(value -> value.getId() != 0)
                .forEach(menu->{
                    if(menu.getParentId()==0){
                        //根节点的孩子,就是我们需要的第一层节点
                        menus.add(menu);
                    }else{
                        //当前节点的父节点
                        SysMenu sysMenu = menuMap.get(menu.getParentId());
                        //判断父节点的孩子集合是否为空,为空创建集合
                        if(CollectionUtil.isEmpty(sysMenu.getChildren())){
                            menuMap.get(sysMenu.getId()).setChildren(new ArrayList<>());
                        }
                        //将当前节点添加到其父节点中
                        sysMenu.getChildren().add(menu);
                    }
                });
        return menus;
    }

}
