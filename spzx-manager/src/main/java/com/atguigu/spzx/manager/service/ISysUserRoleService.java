package com.atguigu.spzx.manager.service;


import com.atguigu.spzx.model.vo.common.dto.system.AssignRolesDto;
import com.atguigu.spzx.model.vo.common.entity.system.SysRole;
import com.atguigu.spzx.model.vo.common.entity.system.SysUserRole;
import com.atguigu.spzx.model.vo.common.vo.system.OperationMenuVo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 用户角色 服务类
 * </p>
 *
 * @author 宋哥
 * @since 2024-01-11
 */

public interface ISysUserRoleService extends IService<SysUserRole> {
    List<Long> findSysRoleByUserId(Long userId);


    void doAssign(AssignRolesDto assignRolesDto);

    List<OperationMenuVo> findOperationOfMenus();
}
