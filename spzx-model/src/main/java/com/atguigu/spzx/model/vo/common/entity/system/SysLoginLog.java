package com.atguigu.spzx.model.vo.common.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统用户登录记录
 * </p>
 *
 * @author 宋哥
 * @since 2023-12-25
 */
@Data

@Accessors(chain = true)
@TableName("sys_login_log")
@ApiModel(value="SysLoginLog对象", description="系统用户登录记录")
public class SysLoginLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "访问ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "用户账号")
    private String username;

    @ApiModelProperty(value = "登录IP地址")
    private String ipaddr;

    @ApiModelProperty(value = "登录状态（0成功 1失败）")
    private Boolean status;

    @ApiModelProperty(value = "提示信息")
    private String msg;

    @ApiModelProperty(value = "访问时间")
    private LocalDateTime accessTime;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标记（0:不可用 1:可用）")
    private Integer isDeleted;


}
