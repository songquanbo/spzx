package com.atguigu.spzx.product.service;

import com.atguigu.spzx.model.vo.common.vo.product.IndexVo;

public interface IIndexService {
    IndexVo index();
}
